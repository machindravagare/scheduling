package ai.wavelabs.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
	@Entity 
	@Table(name = "travellerdetails")
	public class TravellerDetails {
		@Id //@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		int travelid;
		String place;
		 @Max(4000)
		int price;
		int distance;
		
		String transportation;

		public TravellerDetails(int travelid, String place, int price, int distance, String transportation) {
			super();
			this.travelid = travelid;
			this.place = place;
			this.price = price;
			this.distance = distance;
			this.transportation = transportation;
		}

		public TravellerDetails() {

		}

		public int BankgetTravelid() {
			return travelid;
		}

		public void setTravelid(int travelid) {
			this.travelid = travelid;
		}

		public String getPlace() {
			return place;
		}

		public void setPlace(String place) {
			this.place = place;
		}

		public int getPrice() {
			return price;
		}

		public void setPrice(int price) {
			this.price = price;
		}

		public int getDistance() {
			return distance;
		}

		public void setDistance(int distance) {
			this.distance = distance;
		}

		public String getTransportation() {
			return transportation;
		}

		public void setTransportation(String transportation) {
			this.transportation = transportation;
		}
	}


