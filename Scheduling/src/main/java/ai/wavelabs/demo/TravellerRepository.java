package ai.wavelabs.demo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TravellerRepository extends JpaRepository<TravellerDetails, Integer> {

}
